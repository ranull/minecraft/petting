package com.ranull.petting;

import com.ranull.petting.command.PettingCommand;
import com.ranull.petting.listener.EntityDeathListener;
import com.ranull.petting.listener.PlayerInteractEntityListener;
import com.ranull.petting.manager.PettingManager;
import com.ranull.petting.manager.VersionManager;
import org.bukkit.command.PluginCommand;
import org.bukkit.plugin.java.JavaPlugin;

public final class Petting extends JavaPlugin {
    private VersionManager versionManager;
    private PettingManager pettingManager;

    @Override
    public void onEnable() {
        saveDefaultConfig();

        versionManager = new VersionManager(this);
        pettingManager = new PettingManager(this);

        PluginCommand pluginCommand = getCommand("petting");

        if (pluginCommand != null) {
            pluginCommand.setExecutor(new PettingCommand(this));
        }

        getServer().getPluginManager().registerEvents(new PlayerInteractEntityListener(this), this);
        getServer().getPluginManager().registerEvents(new EntityDeathListener(this), this);
    }

    public VersionManager getVersionManager() {
        return versionManager;
    }

    public PettingManager getPettingManager() {
        return pettingManager;
    }
}
