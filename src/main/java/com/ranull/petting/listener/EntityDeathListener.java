package com.ranull.petting.listener;

import com.ranull.petting.Petting;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;

public class EntityDeathListener implements Listener {
    private final Petting plugin;

    public EntityDeathListener(Petting plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onEntityDeath(EntityDeathEvent event) {
        if (plugin.getPettingManager().hasEntity(event.getEntity())) {
            plugin.getPettingManager().removeEntity(event.getEntity());
        }
    }
}
