package com.ranull.petting.listener;

import com.ranull.petting.Petting;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.EquipmentSlot;

public class PlayerInteractEntityListener implements Listener {
    private final Petting plugin;

    public PlayerInteractEntityListener(Petting plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onEntityInteract(PlayerInteractEntityEvent event) {
        Player player = event.getPlayer();

        if ((!plugin.getVersionManager().hasSecondHand() || event.getHand() == EquipmentSlot.HAND)
                && (plugin.getVersionManager().is_v1_7() || player.getGameMode() != GameMode.SPECTATOR)
                && event.getRightClicked() instanceof LivingEntity
                && player.hasPermission("petting.use")
                && (!plugin.getConfig().getBoolean("settings.shift")
                || plugin.getConfig().getBoolean("settings.shift")
                && event.getPlayer().isSneaking())
                && (!plugin.getConfig().getBoolean("settings.hand")
                || plugin.getConfig().getBoolean("settings.hand")
                && player.getItemInHand().getType() == Material.AIR)) {
            LivingEntity livingEntity = (LivingEntity) event.getRightClicked();

            if (plugin.getPettingManager().canPetEntity(livingEntity, player)) {
                plugin.getPettingManager().petEntity(livingEntity, player);

                if (plugin.getConfig().getBoolean("settings.event-cancel")) {
                    event.setCancelled(true);
                }
            }
        }
    }
}
