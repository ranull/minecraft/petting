package com.ranull.petting.command;

import com.ranull.petting.Petting;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class PettingCommand implements CommandExecutor {
    private final Petting plugin;

    public PettingCommand(Petting plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        String version = "1.3";
        String author = "Ranull";

        if (args.length == 0) {
            sender.sendMessage(ChatColor.RED + "❤" + ChatColor.DARK_GRAY + " » " + ChatColor.RED
                    + "Petting " + ChatColor.DARK_GRAY + "v" + version);
            sender.sendMessage(
                    ChatColor.GRAY + "/petting " + ChatColor.DARK_GRAY + "-" + ChatColor.RESET + " Plugin info");

            if (sender.hasPermission("petting.reload")) {
                sender.sendMessage(ChatColor.GRAY + "/petting reload " + ChatColor.DARK_GRAY + "-" + ChatColor.RESET
                        + " Reload plugin");
            }

            sender.sendMessage(ChatColor.DARK_GRAY + "Author: " + ChatColor.GRAY + author);
        } else if (args[0].equals("reload")) {
            if (sender.hasPermission("dualwield.reload")) {
                plugin.reloadConfig();
                sender.sendMessage(ChatColor.RED + "❤" + ChatColor.DARK_GRAY + " » " + ChatColor.RESET
                        + "Reloaded config file.");
            } else {
                sender.sendMessage(ChatColor.RED + "❤" + ChatColor.DARK_GRAY + " » " + ChatColor.RESET
                        + "No Permission.");
            }
        }

        return true;
    }
}