package com.ranull.petting.manager;

import com.ranull.petting.Petting;
import com.ranull.petting.util.ReflectionUtil;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.attribute.Attribute;
import org.bukkit.attribute.AttributeInstance;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Tameable;

import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class PettingManager {
    private final Petting plugin;
    private final ConcurrentMap<UUID, Long> petDelayMap = new ConcurrentHashMap<>();

    public PettingManager(Petting plugin) {
        this.plugin = plugin;
    }

    public void petEntity(LivingEntity livingEntity, Player player) {
        playSound(livingEntity);
        spawnParticle(livingEntity);
        healEntity(livingEntity);
        swingMainHand(player);
    }

    public boolean hasEntity(Entity entity) {
        return petDelayMap.containsKey(entity.getUniqueId());
    }

    public void removeEntity(Entity entity) {
        petDelayMap.remove(entity.getUniqueId());
    }

    public boolean canPetEntity(LivingEntity livingEntity, Player player) {
        if (plugin.getConfig().isSet("settings.sound." + livingEntity.getType().name())) {
            if (livingEntity instanceof Tameable) {
                Tameable tameable = (Tameable) livingEntity;

                if ((plugin.getConfig().getBoolean("settings.tamed")
                        && !tameable.isTamed())
                        || (plugin.getConfig().getBoolean("settings.owner")
                        && !player.equals(tameable.getOwner()))) {
                    return false;
                }
            }

            if (petDelayMap.containsKey(livingEntity.getUniqueId())) {
                if ((System.currentTimeMillis() - petDelayMap.get(livingEntity.getUniqueId()))
                        > plugin.getConfig().getDouble("settings.cooldown") * 1000) {
                    petDelayMap.put(livingEntity.getUniqueId(), System.currentTimeMillis());

                    return true;
                }
            } else {
                petDelayMap.put(livingEntity.getUniqueId(), System.currentTimeMillis());

                return true;
            }
        }

        return false;
    }

    @SuppressWarnings("deprecation")
    private void healEntity(LivingEntity livingEntity) {
        if (plugin.getVersionManager().hasAttribute()) {
            AttributeInstance attributeInstance = livingEntity.getAttribute(Attribute.GENERIC_MAX_HEALTH);

            if (attributeInstance != null) {
                livingEntity.setHealth(Math.min((livingEntity.getHealth() + plugin.getConfig()
                        .getDouble("settings.heal-amount")), attributeInstance.getValue()));
            }
        } else {
            livingEntity.setHealth(Math.min((livingEntity.getHealth() + plugin.getConfig()
                    .getDouble("settings.heal-amount")), livingEntity.getMaxHealth()));
        }
    }

    private void playSound(Entity entity) {
        String string = plugin.getConfig().getString("settings.sound." + entity.getType().name());

        if (string != null && !string.equals("")) {
            try {
                Sound sound = Sound.valueOf(string.toUpperCase());
                float volume = (float) plugin.getConfig().getDouble("settings.volume", 1);
                float pitch = (float) plugin.getConfig().getDouble("settings.pitch", 1);

                entity.getWorld().playSound(entity.getLocation(), sound, volume, pitch);
            } catch (IllegalArgumentException exception) {
                plugin.getLogger().info("Warning: " + string + " is not a Sound ENUM");
            }
        }
    }

    private void spawnParticle(Entity entity) {
        Location location = entity.getLocation().clone().add(0, 1, 0);

        if (plugin.getVersionManager().hasParticle()) {
            entity.getWorld().spawnParticle(Particle.HEART, location, 1);
        } else {
            entity.getWorld().playEffect(location, Effect.valueOf("HEART"), Integer.MAX_VALUE);
        }
    }

    public void swingMainHand(Player player) {
        if (plugin.getVersionManager().hasSwingHand()) {
            player.swingMainHand();
        } else {
            ReflectionUtil.swingMainHand(player);
        }
    }
}
